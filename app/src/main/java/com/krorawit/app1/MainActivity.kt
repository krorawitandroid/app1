package com.krorawit.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val helloButton = findViewById<Button>(R.id.button)
        helloButton.setOnClickListener {
            val intent = Intent(this,HelloActivity::class.java)
            startActivity(intent)

            Log.d("success", findViewById<Button>(R.id.button).toString())
        }
    }
}

